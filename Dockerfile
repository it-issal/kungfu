FROM ubuntu:14.04

RUN ["apt-get", "update"]
RUN ["apt-get", "-y", "upgrade"]
RUN ["apt-get", "clean"]

RUN ["apt-get", "install", "--force-yes", "-y", "wget", "unzip", "git"]

RUN ["mkdir", "-p", "/kata/config"]

COPY etc         /kata/config
COPY res/scripts /kata

WORKDIR /kata

RUN ["scripts/boot.sh"]

RUN ["scripts/clean.sh"]

VOLUME /app

RUN ["scripts/build.sh"]

EXPOSE 80:80

CMD ["scripts/run.sh"]
