import os, ldap
from django_auth_ldap.config import LDAPSearch

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': '/seed/var/puppeteer/main.db',
    }
}

BUILD_DIRS = "/seed/var/puppeteer/builds"

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'

USE_I18N = True
USE_L10N = True
USE_TZ = True

STATIC_ROOT = '/seed/var/puppeteer/static'
STATIC_URL  = '/static/'

GITLAB_TARGET = 'code.maher-pur.pl'
GITLAB_TOKEN  = 'JVNSESs8EwWRx5yDxM5q'

SECRET_KEY = '1-zz91z4f7ckc8re8713u=g#9(&@y9o-a!3qt52g-^)ipek77*'

AUTH_LDAP_SERVER_URI = "ldap://authority.maher-pur.pl"
AUTH_LDAP_BIND_DN = ""
AUTH_LDAP_BIND_PASSWORD = ""
AUTH_LDAP_USER_SEARCH = LDAPSearch("ou=users,dc=maher-pur,dc=pl", ldap.SCOPE_SUBTREE, "(uid=%(user)s)")

BROKER_URL = 'amqp://puppeteer:backed2014@localhost//puppeteer'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

STATICFILES_DIRS = (
    '/seed/usr/share/puppeteer/assets',
)

TEMPLATE_DIRS = (
    '/seed/usr/share/puppeteer/templates',
)

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    
    'django_extensions',
    #'django_evolution',
    #'django_auth_ldap',
    #'django2use',
    'celery',
    
    'puppeteer.core',
    'puppeteer.ops',
    'puppeteer.mgmt',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ALLOWED_HOSTS = ['ci.maher-pur.pl']

ROOT_URLCONF = 'puppeteer.urls'
WSGI_APPLICATION = 'puppeteer.wsgi.application'

TEMPLATE_DEBUG = DEBUG

