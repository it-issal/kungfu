#-*- coding: utf-8 -*-

from puppeteer.shortcuts import *

from puppeteer.mgmt.models import *
from puppeteer.mgmt import tasks

#################################################################################################

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--config',
            action='store_true',
            dest='config',
            default=False,
            help='Configure services')
    
    def handle(self, ntr, *narrow, **options):
        for nrw in narrow:
            lookup = dict(
                nature=ntr,
                alias=nrw
            )
            
            try:
                prj = Project.objects.get(**lookup)
            except Project.DoesNotExist:
                print "No project of name : %s" % nrw
            
            if prj:
                if options.get('config', True):
                    for aspect in ('gitlab', 'web', 'build'):
                        hnd = getattr(tasks, 'config_project_%s' % aspect, None)
                        
                        if callable(hnd):
                            hnd.delay(lookup)

