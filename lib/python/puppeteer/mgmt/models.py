#-*- coding: utf-8 -*-

from puppeteer.shortcuts import *

from puppeteer.core.models import Geek
from puppeteer.ops.models import Build_Slave

#################################################################################################

PROJECT_TYPEs = (
    ('droid',   "Android project"),
    ('app',     "Regular : .NET, J2EE, ..."),
    ('vhost',   "Web-hosted : PHP, WSGI, Passenger, ..."),
    
    ('plant',   "Docker project"),
    ('seed',    "Vagrant project"),
)

PROJECT_BUILD_SLAVEs = {
    'droid': 2,
    'seed': 4,
}

class Project(models.Model):
    owner      = models.ForeignKey(Geek, related_name='projects')
    
    nature     = models.CharField(max_length=16, choices=PROJECT_TYPEs)
    alias      = models.CharField(max_length=128)
    
    title      = models.CharField(max_length=128, blank=True)
    recipe     = models.TextField(blank=True, null=True, default=None)
    
    slaves     = models.ManyToManyField(Build_Slave, blank=True)
    
    repo_name  = property(lambda self: "%(nature)s-%(alias)s" % self.__dict__)
    repo_link  = property(lambda self: "https://%s/%s/%s.git" % (settings.GITLAB_TARGET, self.owner.pseudo, self.repo_name))
    
    @property
    def conn(self):
        if not hasattr(self, '_gitlab'):
            cnx = gitlab.Gitlab(settings.GITLAB_TARGET, settings.GITLAB_TOKEN)
            
            cnx.auth()
            
            setattr(self, '_gitlab', cnx)
        
        return getattr(self, '_gitlab', None)
    
    buildbot_slaves = property(lambda self: PROJECT_BUILD_SLAVEs.get(self.nature, 1))
    
    def __str__(self):     return str(self.user)
    def __unicode__(self): return unicode (self.user)

