# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ops', '0001_initial'),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nature', models.CharField(max_length=16, choices=[(b'droid', b'Android project'), (b'app', b'Regular : .NET, J2EE, ...'), (b'vhost', b'Web-hosted : PHP, WSGI, Passenger, ...'), (b'plant', b'Docker project'), (b'seed', b'Vagrant project')])),
                ('alias', models.CharField(max_length=128)),
                ('title', models.CharField(max_length=128, blank=True)),
                ('recipe', models.TextField(default=None, null=True, blank=True)),
                ('owner', models.ForeignKey(related_name=b'projects', to='core.Geek')),
                ('slaves', models.ManyToManyField(to='ops.Build_Slave', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
