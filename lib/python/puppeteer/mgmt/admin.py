#-*- coding: utf-8 -*-

from django.contrib import admin

from .models import *

#################################################################################################

class ProjectAdmin(admin.ModelAdmin):
    list_display = ['alias', 'nature', 'owner', 'repo_link']
    list_filter  = ['nature', 'owner__user']

admin.site.register(Project, ProjectAdmin)

