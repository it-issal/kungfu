#-*- coding: utf-8 -*-

from puppeteer.shortcuts import *

from .models import *

#################################################################################################

@register_task
def config_project_gitlab(prj):
    resp = None
    
    for prj in prj.conn.Project():
        if prj.name==prj.repo_name:
            resp = prj
    
    if resp is None:
        resp = prj.conn.Project({
            'name':         prj.repo_name,
            'wiki_enabled': False,
        })
        
        resp.save()

#################################################################################################

@register_task
def config_project_web(prj):
    pass

#################################################################################################

@register_task
def config_project_build(prj):
    if not os.path.exists(build_path(prj.repo_name)):
        os.system('mkdir -p %s' % build_path(prj.repo_name))
    
    for key in ['master'] + ['slave-%d' % (s+1) for s in range(0, prj.buildbot_slaves)]:
        if not os.path.exists(build_path(prj.repo_name, key)):
            os.chdir(build_path(prj.repo_name))
            
            if key=='master':
                os.system('buildbot create-master %s' % key)
                
                cookbook_render('buildbot/master.py', build_path(prj.repo_name, key, 'master.cfg'),
                    project  = prj.__dict__,
                    hostname = socket.gethostname(),
                    slaves   = ['slave-%d' % (i+1) for i in range(0, prj.buildbot_slaves)]
                )
            else:
                os.system('buildslave create-slave %s gateway:9989 %s pass' % (key, socket.gethostname()))

