#-*- coding: utf-8 -*-

from puppeteer.shortcuts import *

from .models import *

#################################################################################################

def project_overview(request, ntr, nrw):
    try:
        prj = Project.objects.get(nature=ntr, alias=nrw)
    except Project.DoesNotExist:
        raise Http404
    
    return render_to_response('project/overview.html', dict(
        project = prj,
    ))

