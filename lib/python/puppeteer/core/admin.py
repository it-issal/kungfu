#-*- coding: utf-8 -*-

from django.contrib import admin

from .models import *

#################################################################################################

class GeekAdmin(admin.ModelAdmin):
    list_display = ['user', 'pseudo', 'email', 'since', 'last_time']
    list_filter  = ['user__first_name', 'user__last_name']

admin.site.register(Geek, GeekAdmin)

