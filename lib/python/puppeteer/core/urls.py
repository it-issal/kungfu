from django.conf.urls import patterns, include, url

urlpatterns = patterns('puppeteer.core.views',
    url(r'^assets/(.*)$',                     'assets_1'),
    url(r'^(js|css|fonts|img|images)/(.*)$',  'assets_2'),
    url(r'^(.+).html$',                       'default_skin'),
    url(r'^$',                                'homepage',          name='home'),
)

