#-*- coding: utf-8 -*-

from puppeteer.shortcuts import *

from .models import *

#################################################################################################

def not_found(request):
    return render_to_response('special/not_found.html')

def default_skin(request, path):
    return render_to_response('skin/%s.html' % path)

#################################################################################################

def assets_1(request, path):
    return HttpResponseRedirect('/static/' + path)

def assets_2(request, folder, path):
    return HttpResponseRedirect('/'.join(['/static', folder, path]))

#################################################################################################

def homepage(request):
    from puppeteer.mgmt.models import Project
    
    return render_to_response('global/homepage.html', dict(
        projects = Project.objects.all(),
    ))

