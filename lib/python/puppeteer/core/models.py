#-*- coding: utf-8 -*-

from puppeteer.shortcuts import *

#################################################################################################

class Geek(models.Model):
    user       = models.ForeignKey(User)
    
    pseudo     = property(lambda self: self.user.username)
    email      = property(lambda self: self.user.email)
    
    since      = property(lambda self: self.user.date_joined)
    last_time  = property(lambda self: self.user.date_joined)
    
    is_staff   = property(lambda self: self.user.is_staff())
    is_super   = property(lambda self: self.user.is_superuser)
    is_active  = property(lambda self: self.user.is_active())
    
    def __str__(self):     return str(self.user)
    def __unicode__(self): return unicode (self.user)

