#-*- coding: utf-8 -*-

from puppeteer.shortcuts import *

from .models import *

#################################################################################################

def machine_overview(request, nrw):
    try:
        hst = Machine.objects.get(hostname=nrw)
    except Machine.DoesNotExist:
        raise Http404
    
    return render_to_response('machine/overview.html', dict(
        machine = hst,
    ))

#################################################################################################

def machine_setup(request):
    if request.META['REMOTE_ADDR'].startswith('192.168.33.'):
        narrow = dict(
            hostname  = request.GET.get('host', request.META['REMOTE_HOST']),
        )
        fields = dict(
            priv_addr = request.META['REMOTE_ADDR'],
        )
        
        obj,st = Machine.objects.get_or_create(**narrow)
        
        for key in fields:
            setattr(obj, key, fields[key])
        
        obj.save()
        
        return render_json({
            'status': 'ok',
            'args':   fields,
            'host':   dict([
                (k, obj.__dict__[k])
                for k in obj.__dict__
                if (obj.__dict__[k] is None) or type(obj.__dict__[k]) in [bool, int, long, float, str, unicode, tuple, set, frozenset, list, dict]
            ]),
        })
    else:
        return render_json({
            'status': 'error',
            'reason': "IP Address not accepted.",
            'args': dict(
                address = request.META['REMOTE_ADDR'],
            ),
        })

#################################################################################################

@csrf_exempt
def machine_add_ssh(request):
    if request.META['REMOTE_ADDR'].startswith('192.168.33.'):
        narrow = dict(
            hostname  = request.GET.get('host', request.META['REMOTE_HOST']),
        )
        fields = dict(
            priv_addr = request.META['REMOTE_ADDR'],
        )
        
        obj,st = Machine.objects.get_or_create(**narrow)
        
        for key in fields:
            setattr(obj, key, fields[key])
        
        obj.save()
        
        key,st = SSH_Key.objects.get_or_create(
            machine = obj,
            user    = request.GET.get('user', 'root'),
        )
        
        key.pub_key = request.read()
        
        key.save()
        
        return render_json({
            'status':  'ok',
            'pub_key': key.pub_key,
            'user':    key.user,
            'host':    dict([
                (k, obj.__dict__[k])
                for k in obj.__dict__
                if (obj.__dict__[k] is None) or type(obj.__dict__[k]) in [bool, int, long, float, str, unicode, tuple, set, frozenset, list, dict]
            ]),
        })
    else:
        return render_json({
            'status': 'error',
            'reason': "IP Address not accepted.",
            'args': dict(
                address = request.META['REMOTE_ADDR'],
            ),
        })

