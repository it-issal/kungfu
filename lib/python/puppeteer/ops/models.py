#-*- coding: utf-8 -*-

from puppeteer.shortcuts import *

#################################################################################################

class ClusterRole(models.Model):
    alias      = models.CharField(max_length=48)
    image      = models.CharField(max_length=128)
    
    scale      = models.PositiveIntegerField(default=0)
    mem_ram    = models.PositiveIntegerField(default=512)
    net_offset = models.PositiveIntegerField(default=10)
    
    def is_master(self, hostname):
        return hostname==('%(alias)s-master' % self)
    
    def is_slave(self, hostname):
        return hostname.startswith('%(alias)s-slave-' % self)
    
    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode (self.alias)

#################################################################################################

class Machine(models.Model):
    when       = models.DateTimeField(blank=True, auto_now_add=True)
    hostname   = models.CharField(max_length=128)
    
    priv_addr  = models.IPAddressField(blank=True, null=True, default=None)
    pub_addr   = models.IPAddressField(blank=True, null=True, default=None)
    
    repo_name  = property(lambda self: slugify(self.hostname))
    repo_link  = property(lambda self: "git@%s:fleet/%s.git" % (settings.GITLAB_TARGET, self.repo_name))
    
    @property
    def conn(self):
        if not hasattr(self, '_gitlab'):
            cnx = gitlab.Gitlab(settings.GITLAB_TARGET, settings.GITLAB_TOKEN)
            
            cnx.auth()
            
            setattr(self, '_gitlab', cnx)
        
        return getattr(self, '_gitlab', None)
    
    @property
    def cluster_role(self):
        resp = None
        
        for r in ClusterRole.objects.all():
            if r.is_master(self.hostname):
                resp = r
            elif r.is_slave(self.hostname):
                resp = r
        
        if resp is None:
            pass
        
        return resp
    
    buildbot_slaves = property(lambda self: PROJECT_BUILD_SLAVEs.get(self.nature, 1))
    
    def __str__(self):     return str(self.hostname)
    def __unicode__(self): return unicode (self.hostname)
    
    @property
    def linfo(self):
        try:
            req = requests.get('http://%s:8090/linfo/?out=json' % self.priv_addr)
        except requests.ConnectionError,ex:
            return {}
        
        try:
            return req.json()
        except Exception,ex:
            return req.content

#************************************************************************************************

class SSH_Key(models.Model):
    machine = models.ForeignKey(Machine, related_name='ssh_keys')
    user    = models.CharField(max_length=32)
    pub_key = models.TextField(blank=True)

#################################################################################################

class Build_Slave(models.Model):
    machine = models.ForeignKey(Machine, related_name='buildslaves')

