from django.conf.urls import patterns, include, url

urlpatterns = patterns('puppeteer.ops.views',
    url(r'^-/(.+)$',       'machine_overview',  name='machine_overview'),
    
    url(r'^rest/setup$',   'machine_setup',     name='machine_setup'),
    url(r'^rest/add_ssh$', 'machine_add_ssh',   name='machine_add_ssh'),
)

