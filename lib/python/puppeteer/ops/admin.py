#-*- coding: utf-8 -*-

from django.contrib import admin

from .models import *

#################################################################################################

class RoleAdmin(admin.ModelAdmin):
    list_display  = ['alias', 'image', 'scale', 'mem_ram', 'net_offset']
    list_filter   = ['image', 'scale', 'mem_ram']
    list_editable = ['scale', 'mem_ram']

admin.site.register(ClusterRole, RoleAdmin)

#################################################################################################

class SshKeys(admin.TabularInline):
    model = SSH_Key

class MachineAdmin(admin.ModelAdmin):
    list_display = ['hostname', 'when', 'pub_addr', 'priv_addr', 'linfo']
    list_filter  = ['when']
    
    inlines      = (
        SshKeys,
    )

admin.site.register(Machine, MachineAdmin)

