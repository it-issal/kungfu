# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Build_Slave',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ClusterRole',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('alias', models.CharField(max_length=48)),
                ('image', models.CharField(max_length=128)),
                ('scale', models.PositiveIntegerField(default=0)),
                ('mem_ram', models.PositiveIntegerField(default=512)),
                ('net_offset', models.PositiveIntegerField(default=10)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Machine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('when', models.DateTimeField(auto_now_add=True)),
                ('hostname', models.CharField(max_length=128)),
                ('priv_addr', models.IPAddressField(default=None, null=True, blank=True)),
                ('pub_addr', models.IPAddressField(default=None, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SSH_Key',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user', models.CharField(max_length=32)),
                ('pub_key', models.TextField()),
                ('machine', models.ForeignKey(related_name=b'ssh_keys', to='ops.Machine')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='build_slave',
            name='machine',
            field=models.ForeignKey(related_name=b'buildslaves', to='ops.Machine'),
            preserve_default=True,
        ),
    ]
