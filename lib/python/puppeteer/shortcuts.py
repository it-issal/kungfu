from __future__ import absolute_import

import os, sys, socket

from django import template
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError
from django.db import models
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.utils.text import slugify
from django.views.decorators.csrf import csrf_exempt, csrf_protect

import requests, gitlab
import simplejson as json
from celery import shared_task

from puppeteer import settings

#################################################################################################

def render_json(data):
    resp = HttpResponse(json.dumps(data))
    
    resp['Content-Type'] = 'application/json'
    
    return resp

#################################################################################################

def build_path(*x):
    return os.path.join('/seed/var/puppeteer/builds', *x)

#################################################################################################

def cookbook_load(path):
    return template.loader.get_template(path, dirs=[
        '/seed/usr/share/puppeteer/cookbook',
    ])

def cookbook_save(path, cnt):
    f = open(path, )

def cookbook_render(tpl, target, **cnt):
    tpl = cookbook_load(tpl)
    
    cookbook_save(target, tpl.render(Context(cnt)))

#################################################################################################

class TaskWrapper(object):
    def __init__(self, hnd):
        self._hnd = hnd
    
    handler   = property(lambda self: self._hnd)
    
    __name__  = property(lambda self: self.handler.__name__)
    
    def __call__(self, nrw, *args, **kwargs):
        try:
            obj = Project.objects.get(**nrw)
        except Project.DoesNotExist:
            pass
        
        if obj:
            return self.handler(obj, *args, **kwargs)
        else:
            return None

def register_task(hnd):
    resp = TaskWrapper(hnd)
    
    resp = shared_task(resp)
    
    return resp

