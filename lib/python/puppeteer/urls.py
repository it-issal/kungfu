from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^platform/',  include('puppeteer.ops.urls')),
    url(r'^ci/',        include('puppeteer.mgmt.urls')),
    url(r'^',           include('puppeteer.core.urls')),
)

handler404 = 'puppeteer.core.views.not_found'

