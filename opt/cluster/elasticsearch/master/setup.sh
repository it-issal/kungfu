#!/bin/bash

dpkg -i $ASSEMBLY_DIR/Ubuntu/elasticsearch-$ELASTIC_VERSION.deb

cd /usr/share/elasticsearch

for plg in $ELASTIC_PLUGINs ; do
    bin/plugin --install $plg
done

cp $CLUSTER_CFG/elastic.yaml /etc/elasticsearch/elasticsearch.yml

ufw allow in 9200/tcp
ufw allow in 9300/tcp
