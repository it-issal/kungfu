#!/bin/bash

if [[ ! -e /etc/mongodb.conf ]] ; then
 
   apt-get -y install mongodb 
   cp $CLUSTER_CFG/mongodb.conf /etc/mongodb.conf

fi

ufw allow in from $NET_WAN to any port 27017 proto tcp

