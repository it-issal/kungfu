#!/bin/bash

echo "nameserver srv-ns1"  > /etc/resolvconf/resolv.conf.d/head
echo "nameserver srv-ns2" >> /etc/resolvconf/resolv.conf.d/head
echo "nameserver 8.8.8.8" >> /etc/resolvconf/resolv.conf.d/head

cp /etc/resolvconf/resolv.conf.d/head /etc/resolv.conf

service ntp stop

#ntpdate -v 0.africa.pool.ntp.org
ntpdate -v srv-ntp

service ntp restart

ufw enable

ufw allow in from $NET_WAN to any port 22 proto tcp

