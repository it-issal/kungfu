#!/bin/bash

#source /vagrant/env.sh

apt-get install -y make zlib1g-dev libbz2-dev libssl-dev libncurses5-dev libsqlite3-dev libreadline-dev tk-dev libpcre3-dev libpcre++-dev build-essential g++ libmysqlclient-dev

if [[ ! -d /opt/mozdef ]] ; then
    git clone $MOZDEF_GIT_REPO /opt/mozdef
fi

#cp /vagrant/etc/roles/gateway/mozdef/local.conf /opt/mozdef/etc/local.conf

if [[ -d /opt/mozdef ]] ; then
    cd /opt/mozdef/docker

    make build

    nohup make run
fi

ufw allow in 3000/tcp # meteor
ufw allow in 9090/tcp # kibana
ufw allow in 9200/tcp # elasticsearch
ufw allow in 8080/tcp # loginput
ufw allow in 8081/tcp # rest api

echo Please configure Mozdef by accessing 'http://defender.maher-ops.pl/' using this credentials : admin / admin | python /vagrant/macro.py

