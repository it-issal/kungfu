#!/bin/bash

source /vagrant/env.sh

NEXUS_EXEC=$NEXUS_HOME/bin/jsw/$NEXUS_PLATFORM

if [[ ! -d /var/lib/nexus ]] ; then
    useradd -d /var/lib/nexus -m -r nexus

    usermod -a -G devops nexus
fi

mkdir -p $NEXUS_TEMP_BASE

if [[ ! -d $NEXUS_HOME ]] ; then
    if [[ -f "$ASSEMBLY_DIR/BINs/nexus-$NEXUS_VERSION-bundle.tar.gz" ]] ; then
        cd /var/lib/nexus/temp

        tar xzf $ASSEMBLY_DIR/BINs/nexus-$NEXUS_VERSION-bundle.tar.gz
    fi

    cd /var/lib/nexus

    if [[ -d $NEXUS_TEMP_DIR ]] ; then
        mv $NEXUS_TEMP_DIR $NEXUS_HOME
        mv $SONATYPE_TEMP_DIR $NEXUS_SONATYPE_DIR
    fi
fi

if [[ -d $NEXUS_HOME ]] ; then
    cp -af $COMPONENT_CFG/nexus.properties $NEXUS_HOME/conf/nexus.properties
    cp -af $COMPONENT_CFG/nexus.sh $NEXUS_HOME/bin/nexus
    ln -sf $NEXUS_HOME/bin/nexus /etc/init.d/nexus
fi

chown nexus:nexus $NEXUS_BASE_DIR /etc/init.d/nexus
chmod 775 -R $NEXUS_BASE_DIR /etc/init.d/nexus

chmod +x /etc/init.d/nexus

update-rc.d nexus defaults

service nexus restart

ufw allow in from $NET_DMZ to any port 8010 proto tcp

echo Please configure Nexus by accessing : http://maven.myopla-pur.pl/

