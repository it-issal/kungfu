#!/bin/bash

apt-get remove --force-yes -y openjdk-6-jre-{headless,lib}

if [[ ! -d $GERRIT_HOMEDIR ]] ; then
    useradd -d $GERRIT_HOMEDIR -m -r gerrit

    usermod -a -G devops gerrit
fi

if [[ ! -d $GERRIT_WORKSPACE ]] ; then
    sudo -u gerrit -H -- java -jar $ASSEMBLY_DIR/Java/gerrit-$GERRIT_VERSION.war init -d $GERRIT_WORKSPACE
fi  

if [[ ! -f /etc/default/gerritcodereview ]] ; then
    echo "GERRIT_SITE=$GERRIT_WORKSPACE" >/etc/default/gerritcodereview
fi

if [[ -d $GERRIT_WORKSPACE ]] ; then
    cp -f $COMPONENT_CFG/gerrit.conf $GERRIT_WORKSPACE/etc/gerrit.config
    cp -f $COMPONENT_CFG/secure.conf $GERRIT_WORKSPACE/etc/secure.config
    #render_file "gerrit.conf" $GERRIT_WORKSPACE/etc/gerrit.config

    ln -sf $GERRIT_WORKSPACE/bin/gerrit.sh /etc/init.d/gerrit

    ufw allow in from $NET_DMZ to any port 8081 proto tcp
    ufw allow in from $NET_WAN to any port 29418 proto tcp

    echo Please configure Gerrit by accessing : http://review.myopla-pur.pl/
fi
