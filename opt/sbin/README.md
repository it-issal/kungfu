HOW TO :
========

Create a repository, assuming it's cloned at '/stack/ci', and put in it a file named 'env.sh' :

```bash
#!/bin/bash

export ASSEMBLY_DIR="/cookbook/cache/packages"
export TARGET_DIR="/stack/ci"

export DEFAULT_PASSWORD="alicebob"

export CB_CLUSTERs="mongodb"
export CB_COMPONENTs="gateway devops builder"

source /cookbook/env.sh
```

For the setup process, use the following script in your repository, eg. as 'setup.sh' :

```bash
#!/bin/bash

source /stack/ci/env.sh

/stack/ci/trigger setup
```

Vagrant integration :
=====================

Add the following lines to your 'Vagrantfile' to ensure the usage of the cookbook on the vagrant machines :

```ruby
    config.vm.synced_folder "/stack",    "/stack",    owner: "root"
    config.vm.synced_folder "/cookbook", "/cookbook", owner: "root"
```

You can also use the inline shell provisionning, as demonstrated below :

```ruby
    config.vm.provision "shell", inline: "echo newhostname >/etc/hostname"
    config.vm.provision "shell", inline: <<EOF
#!/bin/bash

export ASSEMBLY_DIR="/cookbook/cache/packages"
export TARGET_DIR="/stack/ci"

export DEFAULT_PASSWORD="alicebob"

export CB_CLUSTERs="mongodb"
export CB_COMPONENTs="gateway devops builder"

source /stack/ci/env.sh

/stack/ci/trigger setup
EOF
```

