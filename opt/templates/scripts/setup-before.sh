source /etc/lsb-release

#apt-get install -y build-essential # curl
apt-get install -y build-essential # curl

#if [[ -f /usr/bin/curl ]] ; then
#    curl -XGET "http://ops.maher.technology/os/ubuntu/sources.list?distro="$DISTRIB_CODENAME > /etc/apt/sources.list
#else
    echo "deb http://archive.ubuntu.com/ubuntu/ "$DISTRIB_CODENAME" main restricted universe multiverse"                > /etc/apt/sources.list
    echo "deb-src http://archive.ubuntu.com/ubuntu/ "$DISTRIB_CODENAME" main restricted universe multiverse"           >> /etc/apt/sources.list

    echo "deb http://archive.ubuntu.com/ubuntu/ "$DISTRIB_CODENAME"-updates main restricted universe multiverse"       >> /etc/apt/sources.list
    echo "deb-src http://archive.ubuntu.com/ubuntu/ "$DISTRIB_CODENAME"-updates main restricted universe multiverse"   >> /etc/apt/sources.list

    echo "deb http://archive.ubuntu.com/ubuntu/ "$DISTRIB_CODENAME"-backports main restricted universe multiverse"     >> /etc/apt/sources.list
    echo "deb-src http://archive.ubuntu.com/ubuntu/ "$DISTRIB_CODENAME"-backports main restricted universe multiverse" >> /etc/apt/sources.list

    echo "deb http://security.ubuntu.com/ubuntu "$DISTRIB_CODENAME"-security main restricted universe multiverse"         >> /etc/apt/sources.list
    echo "deb-src http://security.ubuntu.com/ubuntu "$DISTRIB_CODENAME"-security main restricted universe multiverse"     >> /etc/apt/sources.list

    echo "deb http://archive.canonical.com/ubuntu "$DISTRIB_CODENAME" partner"                                            >> /etc/apt/sources.list
    echo "deb-src http://archive.canonical.com/ubuntu "$DISTRIB_CODENAME" partner"                                        >> /etc/apt/sources.list

    #echo "deb http://extras.ubuntu.com/ubuntu "$DISTRIB_CODENAME" main"                                                  >> /etc/apt/sources.list
#fi

#echo 'Acquire::http::Proxy "'$http_proxy'";' > /etc/apt/apt.conf.d/30proxy

apt-get update

export DEBIAN_FRONTEND=noninteractive

if [[ -f /var/lib/dpkg/lock ]] ; then
    rm /var/lib/dpkg/lock
fi

dpkg --configure -a

if [[ -f /var/lib/apt/lists/lock ]] ; then
    rm /var/lib/apt/lists/lock
fi

if [[ ! -f /usr/bin/htop ]] ; then
    apt-get update
    apt-get install -y language-pack-fr aptitude python-{software-properties,pip,setuptools,virtualenv}

    add-apt-repository -y ppa:chris-lea/node.js
    add-apt-repository -y ppa:nilarimogard/webupd8
    #add-apt-repository -y ppa:it-samurai/squad

    apt-get update

    PKGs="ccze htop unzip tree ntpdate"
    PKGs=$PKGs" supervisor collectd collectd-utils miredo"
    PKGs=$PKGs" curl build-essential git-all cython"
    PKGs=$PKGs" "`echo {lib{perl,event,mysqlclient,xml2,xslt1},php5,python,ruby1.9.1}-dev`
    PKGs=$PKGs" php5-cli nodejs ruby "`echo openjdk-7-{jre-headless,jdk}`
    PKGs=$PKGs" python-pip rubygems"
    PKGs=$PKGs" apache2 libapache2-mod-php5"

    apt-get install -y --force-yes $PKGs

    unset UCF_FORCE_CONFFOLD
    export UCF_FORCE_CONFFNEW=YES
    ucf --purge /boot/grub/menu.lst

    apt-get -o Dpkg::Options::="--force-confnew" --force-yes -fuy dist-upgrade
fi

if [[ ! -f /usr/local/bin/http ]] ; then
    pip install bpython httpie netifaces
fi

groupadd -r devops

service apparmor stop
service apparmor teardown
update-rc.d -f apparmor remove

