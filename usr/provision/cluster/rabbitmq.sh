rabbitmqctl add_user $MQ_USER "$MQ_PASS"
rabbitmqctl add_vhost $MQ_VHOST
rabbitmqctl set_permissions -p $MQ_VHOST $MQ_USER '.*' '.*' '.*'
