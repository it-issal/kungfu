# Shell commands #

* voodoo
* zombie

* kitchen
* virtctl

# Features #

* Flavor detection on repos dynamically under specific path naming convention.
* C.I & DevOps oriented annotation of vhosts (CDN / API / Mail / CRON / Instances / Bots / Backends / Shipping / ACL).

* Hybrid API in Python for a hybrid cloud management (public / private / on-demand / backyard) with a VM kitchen.
* Provisionning of templates / machines using : API / Commndline / CloudFormation

# Run in Docker #

To run inside Docker:

```
docker pull it2use/kungfu

docker run -p 8081:80 -v /srv/vhost/example.tld:/app --name vhost-example.tld it2use/kungfu
```

# List of packages #

### Virtualization : ###

* Docker
* LXC containers
* UML (User-Mode Linux)

* VirtualBox

* Vagrant

### Cloud solutions : ###

* OpenStack
* Salt Stack

### Cloud providers : ###

* Amazon EC2
* OvH Hosting & APIs.

### Hosting ###

* apache2

* tomcat7
* jboss-as4

* JAX-RS
* grails
* rails

* gunicorn
